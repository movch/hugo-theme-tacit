# Hugo Tacit Theme

![screenshot](https://raw.githubusercontent.com/movch/hugo-tacit-theme/master/images/screenshot.png)

Tacit Theme is a minimalist blog theme for [Hugo](https://gohugo.io/) static site generator for those who just want to publish their texts on web without extra moves. It was combined from [tacit](https://github.com/yegor256/tacit) CSS framework by @yegor256 and [blank](https://github.com/vimux/blank) theme by @vimux with a little bit of custom CSS for better look. It doesn't require any configuration, so you can just drop it to your themes folder and start writing.

## Installation

In the root directory of the Hugo site run:

    git clone https://github.com/movch/hugo-tacit-theme themes/hugo-tacit-theme

Next, open `config.toml` in the root directory of the Hugo site and ensure the theme option is set to `hugo-tacit-theme`.

    theme = "hugo-tacit-theme"

For more information refer the official [setup guide](//gohugo.io/overview/installing/) of Hugo.

## Usage

### Content summaries

Tacit theme shows full posts by default, if you want to truncate your post, you have to add `<!-- more -->` divider to each post manually.

## Contributing

Feel free to open an issue or send a pull request.

## License

Project is distributing under the [MIT license](//github.com/movch/hugo-tacit-theme/blob/master/LICENSE.md).